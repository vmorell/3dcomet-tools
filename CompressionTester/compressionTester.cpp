#include <iostream>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/search/kdtree.h>
#include <pcl/filters/filter.h>

#include <boost/filesystem.hpp>
#include <boost/filesystem.hpp>
#include <boost/random.hpp>
#include <boost/generator_iterator.hpp>

typedef boost::minstd_rand base_generator_type;


namespace fs = ::boost::filesystem;
using namespace std;

//structure to return all error values in cloud comparision
struct cloud_diff{
  float dist_error;
  float color_error;
  float dist_variance;
  float color_variance;
  cloud_diff(){
    dist_error=0;
    color_error=0;
    dist_variance=0;
    color_variance=0;
  };
};

// return the filenames of all files that have the specified extension
// in the specified directory and all subdirectories
void get_all(const fs::path& root, const string& ext, vector<fs::path>& ret)
{  
  if (!fs::exists(root)) return;

  if (fs::is_directory(root))
  {
    fs::recursive_directory_iterator it(root);
    fs::recursive_directory_iterator endit;
    while(it != endit)
    {
      if (fs::is_regular_file(*it) && it->path().extension() == ext)
      {
        ret.push_back(it->path());
      }
      ++it;
    }
  }
}


//Compares two pointclouds and returns the errors in 3D and color distances.
cloud_diff comparePointClouds(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_original, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_reconstructed)
{
  int ir;
  //int is;
  double sumdistancias=0,sumdistanciastotal=0;
  double sumdistanciascolor=0,sumdistanciascolortotal=0;
  double distmediacercano=0;
  double distmediacercanocolor=0;
  double variancedist=0;
  double variancecolor=0;
  double dist=0,col=0;
  pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGBA>);

  std::vector<int> pointIdxNKNSearch(1);
  std::vector<float> pointNKNSquaredDistance(1);

  tree->setInputCloud (cloud_reconstructed);
  sumdistancias=0;
  sumdistanciascolor=0;
  for(ir=0; ir < cloud_original->size(); ir++){
    if(tree->nearestKSearch(cloud_original->points[ir],1,pointIdxNKNSearch, pointNKNSquaredDistance)>0){
      dist=sqrt(pointNKNSquaredDistance.at(0));
      col=sqrt(pow( cloud_original->points[ir].r - cloud_reconstructed->points[pointIdxNKNSearch[0]].r,2.0f)+
        pow( cloud_original->points[ir].g - cloud_reconstructed->points[pointIdxNKNSearch[0]].g,2.0f)+
        pow( cloud_original->points[ir].b - cloud_reconstructed->points[pointIdxNKNSearch[0]].b,2.0f));

      sumdistancias += dist;
      variancedist+= (dist*dist);

      sumdistanciascolor+=col;
      variancecolor+=(col*col);
    }else{
      std::cout << "ERROR, Closest point not found!" <<std::endl;
    }

  }
  sumdistanciastotal+=sumdistancias;
  sumdistanciascolortotal+=sumdistanciascolor;

  distmediacercano=sumdistancias/(cloud_original->size());
  distmediacercanocolor=sumdistanciascolor/(cloud_original->size());

  variancedist/=(cloud_original->size());
  variancedist-=(distmediacercano*distmediacercano);

  variancecolor/=(cloud_original->size());
  variancecolor-=(distmediacercanocolor*distmediacercanocolor);

  cloud_diff error;
  error.dist_error=distmediacercano;
  error.color_error= distmediacercanocolor;
  error.dist_variance = variancedist;
  error.color_variance = variancecolor;
  return error;

}


cloud_diff readAndCompare(std::string original_f, std::string recons_f){

  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_reconstructed (new pcl::PointCloud<pcl::PointXYZRGBA>), cloud_original (new pcl::PointCloud<pcl::PointXYZRGBA>);

  if (pcl::io::loadPCDFile (original_f, *cloud_original) < 0)
  {
    PCL_ERROR ("Error loading cloud %s.\n", original_f.c_str());
    return cloud_diff();
  }

  if (pcl::io::loadPCDFile (recons_f, *cloud_reconstructed) < 0)
  {
    PCL_ERROR ("Error loading cloud %s.\n", recons_f.c_str());
    return cloud_diff();
  }

  //removing NaNs from the pointclouds
  cloud_original->is_dense=false;
  std::vector<int> index;
  pcl::removeNaNFromPointCloud(*cloud_original,*cloud_original,index);

  cloud_reconstructed->is_dense=false;
  pcl::removeNaNFromPointCloud(*cloud_reconstructed,*cloud_reconstructed,index);

  return comparePointClouds(cloud_original, cloud_reconstructed);
}


int
  main (int argc, char** argv)
{
  if(argc < 5){
    std::cout << "Invalid number of parameters \n \t usage: compressionTester general_path original_folder compressed_folder reconstructed_folder <results_file_name> "<<std::endl;
    return 0;
  }
  fs::path dataset_path= argv[1];
  std::string original_filename= argv[2];
  std::string  compressed_filename= argv[3];
  std::string  reconstructed_filename= argv[4];

  fs::path ori_path= dataset_path / original_filename;
  fs::path recons_path= dataset_path / reconstructed_filename;
  fs::path comp_path= dataset_path / compressed_filename;

  string resultfile;
  if(argc>5)
    resultfile=argv[5];
  else
    resultfile="errors.csv";

  std::ofstream fileErrors(resultfile);

  vector<fs::path> originals;
  vector<fs::path> reconstructeds;
  vector<fs::path> compresseds;

  get_all(ori_path, ".pcd", originals);
  get_all(recons_path, ".pcd", reconstructeds);
  get_all(comp_path, "", compresseds);

  fileErrors <<"file;compressionrate;distance error;error var;color error;color var"<<endl;
  bool found=false;
  int origpathsize=ori_path.string().size();
  for(int i=0 ; i< originals.size(); ++i){
    fs::path p =originals[i];
    std::string so =  p.string().substr(origpathsize+1,p.string().size());
    std::string so_without_ext =  so.substr(0,so.size()-4);
    found=false;
    fileErrors <<p <<";";
    std::cout << "cloud: " << p <<" ";

    //search for compressed file
    int comppathsize=comp_path.string().size();
    for(int j=0 ; j< compresseds.size(); ++j){
      fs::path pc =compresseds[j];
      std::string sc =  pc.string().substr(comppathsize+1,pc.string().size());
      if(so_without_ext==sc)
      {
        found=true;
        uintmax_t original_size= boost::filesystem::file_size(p.string());
        uintmax_t compressed_size= boost::filesystem::file_size(pc.string());
        float compression_ratio= compressed_size/(float)original_size;
        cout<< " compressionratio:"<<compression_ratio<<" ";
        fileErrors <<compression_ratio <<";";
        break;
      }

    }
    if(!found){
      std::cout << " :compressed file not found: ";
      fileErrors << "1.0" <<";";
    }

    //search for reconstructed file
    found=false;
    int reconpathsize=recons_path.string().size();
    for(int j=0 ; j< reconstructeds.size(); ++j){
      fs::path pr =reconstructeds[j];
      std::string sr =  pr.string().substr(reconpathsize+1,pr.string().size());
      if(p.filename() == pr.filename() && so==sr)
      {
        found=true;
        cloud_diff error= readAndCompare(p.string(),pr.string());
        cout <<"error dist: "<<error.dist_error<<" var: "<<error.dist_variance  <<" color: "<<error.color_error<<" color var: "<<error.color_variance <<endl;
        fileErrors <<error.dist_error <<";"<<error.dist_variance<<";"<<error.color_error<<";"<<error.color_variance <<endl;
        break;
      }

    }
    if(!found){
      std::cout << ": reconstructed file not found for cloud: "<<endl;
      fileErrors << "-;-;-;-" <<endl;
    }

  }

  return 0;
}




