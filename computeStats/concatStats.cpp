/* 
 * File:   concatStats.cpp
 * Author: iuii
 *
 * Created on 8 de julio de 2015, 10:56
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>


using namespace std;

vector<string> explode(const string& str, const char& ch) {
    string next;
    vector<string> result;

    // For each character in the string
    for (string::const_iterator it = str.begin(); it != str.end(); it++) {
        // If we've hit the terminal character
        if (*it == ch) {
            // If we have some characters accumulated
            if (!next.empty()) {
                // Add them to the result vector
                result.push_back(next);
                next.clear();
            }

        } else {
            // Accumulate the next character into the sequence
            next += *it;
        }
    }
    if (!next.empty())
        result.push_back(next);
    return result;
}

/// print info message
void printInfo() {
    std::cout << " Concat output of compressionTester with dataset statistics. " << endl;
    std::cout << " Usage:" << " concatStats  <output_of_compressionTester> <file_of_all_dataset_statistics>  " << endl;
}

/**
 *  Concatenate output of compressionTester with statistics of datasets
 **/
int main(int argc, char** argv) {

    ifstream in_stream1, in_stream2;
    string line;
    //max files in dataset 500
    vector<string> list1[500], list2[500];
    int i = 0, total_lineas = 0, total_lineas2 = 0;

    //load output of compressionTester
    in_stream1.open(argv[1]);
    if(!in_stream1.good()){
        printInfo();
        return 0;
    }
    while (getline(in_stream1, line)) {
        //in_stream1 >> line;
        list1[i++] = explode(line, ';');
    }
    in_stream1.close();
    total_lineas = i;

    //load statistics from dataset
    i = 0;
    in_stream2.open(argv[2]);
    if(!in_stream2.good()){
        printInfo();
        return 0;
    }
    while (getline(in_stream2, line)) {
        list2[i++] = explode(line, ';');
        //ensure all data 
        for(int j=list2[i-1].size();j<9;j++) list2[i-1].push_back("-");
    }
    in_stream2.close();
    total_lineas2 = i;

    //concat all data
    for (int i = 1; i < total_lineas; i++) {
        //extract name of file
        std::vector<string> auxpath = explode(list1[i][0], '/');

        string fichero = auxpath.at((int) auxpath.size() - 1);
        fichero.erase(std::remove(fichero.begin(), fichero.end(), '\"'), fichero.end());
        fichero.erase(std::remove(fichero.begin(), fichero.end(), ' '), fichero.end());

        // find filename in statistics file
        for (int j = 0; j < total_lineas2; j++) {
            std::size_t found = list2[j][0].find(fichero);
            if (found != std::string::npos) {
                //concatenate both outputs
                cout << list1[i][0];
                for (int k = 1; k < list1[i].size(); k++) {
                    cout << ";" << list1[i][k];
                }
                for (int k = 1; k < list2[j].size(); k++) {
                    cout << ";" << list2[j][k];
                }
                cout << endl;
                break;
            }

        }

    }


    return 0;
}



