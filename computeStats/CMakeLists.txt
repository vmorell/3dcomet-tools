###########################################################################
# <project> Compute statistics  </summary>
# <summary> CMakeLists.txt . </summary>
# <date>    2014-10-01         </date>
# <author>  Javier Navarrete </author>
# <email>  javi.navarrete@ua.es  </email>
############################################################################
cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(computeStats)

find_package(PCL 1.7 REQUIRED)

include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

set(CMAKE_CXX_FLAGS "  -std=c++0x ")

find_package( OpenCV 2.4 REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} )

add_executable (computeStats computeStats.cpp )
target_link_libraries (computeStats ${PCL_LIBRARIES} ${OpenCV_LIBRARIES}    )

add_executable (concatStats concatStats.cpp )
target_link_libraries (concatStats ${Boost_LIBRARIES}    )
