#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d.h>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/core/core_c.h"
#include <stdlib.h>
#include <stdio.h>
using namespace std;
using namespace cv;


typedef pcl::PointXYZRGB PointType;
float k_radio = 0.1;

double
computeCloudResolution(const pcl::PointCloud<PointType>::ConstPtr &cloud) {
    double res = 0.0;
    int n_points = 0;
    int nres;
    std::vector<int> indices(2);
    std::vector<float> sqr_distances(2);
    pcl::search::KdTree<PointType> tree;
    tree.setInputCloud(cloud);

    for (size_t i = 0; i < cloud->size(); ++i) {
        if (!pcl_isfinite((*cloud)[i].x)) {
            continue;
        }
        //Considering the second neighbor since the first is the point itself.
        nres = tree.nearestKSearch(i, 2, indices, sqr_distances);
        if (nres == 2) {
            res += sqrt(sqr_distances[1]);
            ++n_points;
        }
    }
    if (n_points != 0) {
        res /= n_points;
    }
    return res;
}

double
computeCloudResolutionStd(const pcl::PointCloud<PointType>::ConstPtr &cloud, const double cloudResolution) {
    double res = 0.0;
    int n_points = 0;
    int nres;
    std::vector<int> indices(2);
    std::vector<float> sqr_distances(2);
    pcl::search::KdTree<PointType> tree;
    tree.setInputCloud(cloud);

    for (size_t i = 0; i < cloud->size(); ++i) {
        if (!pcl_isfinite((*cloud)[i].x)) {
            continue;
        }
        //Considering the second neighbor since the first is the point itself.
        nres = tree.nearestKSearch(i, 2, indices, sqr_distances);
        if (nres == 2) {
            // std deviation
            double aux = (sqrt(sqr_distances[1]) - cloudResolution) * (sqrt(sqr_distances[1]) - cloudResolution);
            res += aux;
            ++n_points;
        }
    }
    if (n_points != 0) {
        res /= n_points;
    }
    return sqrt(res);
}

double computeCloudCurvature(const pcl::PointCloud<pcl::Normal>::ConstPtr &cloud) {
    double ret = 0;
    int n_points = 0;
    for (size_t i = 0; i < cloud->size(); ++i) {
        if (pcl_isfinite(cloud->points[i].curvature) && !pcl_isinf(cloud->points[i].curvature) && !pcl_isnan(cloud->points[i].curvature)) {
            if (cloud->points[i].curvature > 0)
                ret += cloud->points[i].curvature;
            else
                ret += (-cloud->points[i].curvature);
            ++n_points;
        }
    }
    if (n_points != 0) {
        ret /= n_points;
    }
    return ret;
}

double computeCloudCurvatureStd(const pcl::PointCloud<pcl::Normal>::ConstPtr &cloud, double cloudCurvature) {
    double ret = 0;
    int n_points = 0;
    for (size_t i = 0; i < cloud->size(); ++i) {
        if (pcl_isfinite(cloud->points[i].curvature) && !pcl_isinf(cloud->points[i].curvature) && !pcl_isnan(cloud->points[i].curvature)) {

            ret += (cloud->points[i].curvature - cloudCurvature)*(cloud->points[i].curvature - cloudCurvature);
            ++n_points;
        }
    }

    if (n_points != 0) {
        ret /= n_points;
    }
    return sqrt(ret);
}

double computeCloudRoughness(const pcl::PointCloud<PointType>::ConstPtr &cloud) {
    double ret = 0;
    double D = 0;
    int n_points = 0;
    Eigen::Vector4f plane_parameters;
    float curvature = 0;
    std::vector<int> indices;
    std::vector<float> indicesqr;
    pcl::search::KdTree<PointType> tree;
    tree.setInputCloud(cloud);
    pcl::NormalEstimation<PointType, pcl::Normal> ne;
    ne.setInputCloud(cloud);

    for (size_t i = 0; i < cloud->points.size(); ++i) {
        tree.radiusSearch(i, k_radio, indices, indicesqr);
        ne.computePointNormal(*cloud, indices, plane_parameters, curvature);


        D = (cloud->points[i].x * plane_parameters[0]) +(cloud->points[i].y * plane_parameters[1]) + (cloud->points[i].z * plane_parameters[2]) + plane_parameters[3];
        D = D / sqrt((plane_parameters[0] * plane_parameters[0]) + (plane_parameters[1] * plane_parameters[1]) + (plane_parameters[2] * plane_parameters[2]));
        if (D < 0)D = -D;
        if (pcl_isfinite(D) && !pcl_isinf(D) && !pcl_isnan(D)) {
            ret += D;
            ++n_points;
        }

    }
    if (n_points != 0) {
        ret /= n_points;
    }
    return ret;
}

double computeCloudRoughnessStd(const pcl::PointCloud<PointType>::ConstPtr &cloud, double cloudRoughness) {
    double ret = 0;
    double D = 0;
    int n_points = 0;
    Eigen::Vector4f plane_parameters;
    float curvature = 0;
    std::vector<int> indices;
    std::vector<float> indicesqr;
    pcl::search::KdTree<PointType> tree;
    tree.setInputCloud(cloud);
    pcl::NormalEstimation<PointType, pcl::Normal> ne;
    ne.setInputCloud(cloud);

    for (size_t i = 0; i < cloud->points.size(); ++i) {
        tree.radiusSearch(i, k_radio, indices, indicesqr);
        ne.computePointNormal(*cloud, indices, plane_parameters, curvature);


        D = (cloud->points[i].x * plane_parameters[0]) +(cloud->points[i].y * plane_parameters[1]) + (cloud->points[i].z * plane_parameters[2]) + plane_parameters[3];
        D = D / sqrt((plane_parameters[0] * plane_parameters[0]) + (plane_parameters[1] * plane_parameters[1]) + (plane_parameters[2] * plane_parameters[2]));
        if (D < 0)D = -D;
        if (pcl_isfinite(D) && !pcl_isinf(D) && !pcl_isnan(D)) {
            ret += (D - cloudRoughness)*(D - cloudRoughness);
            ++n_points;
        }

    }

    if (n_points != 0) {
        ret /= n_points;
    }
    return sqrt(ret);
}

double computeCloudEntropy(const pcl::PointCloud<PointType>::ConstPtr &cloud) {

    double sumavalent = 0;
    double entropia = 0;
    double histograma_ent[256];
    double posh;
    int histograma[256];

    //init histogram 0
    for (int i = 0; i < 256; i++) histograma[i] = 0;
    // calc histogram
    for (int id = 0; id < cloud->points.size(); ++id) {
        uint8_t r, g, b;

        r = cloud->points[id].r;
        g = cloud->points[id].g;
        b = cloud->points[id].b;
        posh = (((int) r) * 0.3) +(((int) g) * 0.59) + (((int) b) * 0.11);
        if (posh < 0) posh = 0;
        if (posh > 255) posh = 255;
        histograma[(int) posh]++;
    }

    // calc entropy  
    for (int t = 0; t < 256; t++) //total points
        sumavalent += histograma[t];
    for (int t = 0; t < 256; t++) //normalization
        histograma_ent[t] = histograma[t] / sumavalent;
    for (int t = 0; t < 256; t++) {//calc of entropy
        if (histograma_ent[t] > 0)
            entropia = entropia + (histograma_ent[t]) * (log(histograma_ent[t]));
    }
    entropia = -entropia / log(2);

    return entropia;

}

double computeEdgeness(const char* filename) {
    /// variables

    Mat src, src_gray, dst_gray;
    Mat dst, detected_edges;

    int edgeThresh = 1;
    int lowThreshold = 40; //threshold fijado por Sergio.
    int const max_lowThreshold = 100;
    int ratio = 3;
    int kernel_size = 3;
    /// Load an image
    src = imread(filename);

    if (!src.data) {
        return -1;
    }

    /// Create a matrix of the same type and size as src (for dst)
    dst.create(src.size(), src.type());

    /// Convert the image to grayscale
    cvtColor(src, src_gray, CV_BGR2GRAY);


    /// Reduce noise with a kernel 3x3
    blur(src_gray, detected_edges, Size(3, 3));

    /// Canny detector
    Canny(detected_edges, detected_edges, lowThreshold, lowThreshold*ratio, kernel_size);

    /// Using Canny's output as a mask, we display our result
    dst = Scalar::all(0);

    src.copyTo(dst, detected_edges);

    /// Convert the image to Gray
    cvtColor(dst, dst_gray, CV_RGB2GRAY);

    float total = (dst.rows)*(dst.cols);
    float nozero = countNonZero(dst_gray);
    return nozero / total;

}

/// print info message
void printInfo() {
    std::cout << " Print statistics of a PCL point-cloud:" << endl;
    std::cout << "    - filename of pointcloud" << endl;
    std::cout << "    - resolution, standard deviation of resolution " << endl;
    std::cout << "    - curvature, standard deviation of curvature " << endl;
    std::cout << "    - roughness, standard deviation of roughness " << endl;
    std::cout << "    - entropy" << endl;
    std::cout << "    - edgeness " << endl;
    std::cout << " Usage:" << " computeStats  <file_pointcloud>" << endl;
}

/// 
/**
 *  Generates statistics values from a point cloud file
 *    - filename of point-cloud
 *    - resolution of pointcloud
 *    - standard deviation of pointcloud
 *    - curvature
 *    - standard deviation of curvature
 *    - roughness
 *    - standard deviation of roughness
 *    - entropy
 *    - edgeness
 **/
int main(int argc, char ** argv) {

    if (argc != 2) {
        printInfo();
        return 0;
    }

    pcl::PointCloud<PointType>::Ptr cloud_in(new pcl::PointCloud<PointType>); // cloud in
    pcl::PointCloud<PointType>::Ptr cloud(new pcl::PointCloud<PointType>); // cloud in filtered
    
    ifstream test_stream;
    test_stream.open(argv[1]);
    if(!test_stream.good()){
        printInfo();
        return 0;
    }   
    test_stream.close();
    
    
    //load cloud
    pcl::io::loadPCDFile(argv[1], *cloud_in);

    std::vector<int> indices;
    // Remove Nan points
    pcl::removeNaNFromPointCloud(*cloud_in, *cloud, indices);

    //compute resolution and deviation
    double resolution, stdev;
    resolution = computeCloudResolution(cloud);
    stdev = computeCloudResolutionStd(cloud, resolution);

    // Create the normal estimation class, and pass the input dataset to it
    pcl::NormalEstimation<PointType, pcl::Normal> ne;
    ne.setInputCloud(cloud);

    // Create an empty kdtree representation, and pass it to the normal estimation object.
    // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
    pcl::search::KdTree<PointType>::Ptr tree(new pcl::search::KdTree<PointType> ());
    ne.setSearchMethod(tree);

    // Output datasets
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>);

    // Use all neighbors in a sphere of radius 3cm
    ne.setRadiusSearch(k_radio);

    // Compute the features
    ne.compute(*cloud_normals);

    double curvature = 0, std_curvature = 0;
    curvature = computeCloudCurvature(cloud_normals);
    std_curvature = computeCloudCurvatureStd(cloud_normals, curvature);

    double roughness = 0, std_roughness;
    roughness = computeCloudRoughness(cloud);
    std_roughness = computeCloudRoughnessStd(cloud, roughness);

    double entropy = 0;
    entropy = computeCloudEntropy(cloud);

    double edgeness = 0;
    string filename;
    filename = string(argv[1]) + string(".png");
    edgeness = computeEdgeness(filename.c_str());

    std::cout << argv[1] << ";" << resolution << ";" << stdev << ";" << curvature << ";" << std_curvature << ";" << roughness << ";" << std_roughness << ";" << entropy;
    if (edgeness < 0)
        std::cout << ";" << std::endl;
    else
        std::cout << ";" << edgeness << std::endl;


    return 0;
}