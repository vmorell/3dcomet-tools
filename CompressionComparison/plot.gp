file_exists(file) = system("[ -f '".file."' ] && echo '1' || echo '0'") + 0
set datafile separator ";"

if (exists("t")) {
    if (t eq "pdf") {
        set terminal pdfcairo font "Arial,14"
    }
    else {
        set terminal t  font "Arial,14"
    }
}

if (exists("f1")) {
    if (file_exists(f1)) {
        files = f1." "
        if (exists("f2")) {
            if (file_exists(f2)) {
                files = files.f2." "
                if (exists("f3")) {
                    if (file_exists(f3)) {
                        files = files.f3." "
                        if (exists("f4")) {
                            if (file_exists(f4)) {
                                files = files.f4." "
                                if (exists("f5")) {
                                    if (file_exists(f5)) {
                                        files = files.f5." "
                                        if (exists("f6")) {
                                            if (file_exists(f6)) {
                                                files = files.f6." "

                                            }
                                            else {
                                                print "File ".f6." does not exist"
                                                quit
                                            }
                                        }
                                    }
                                    else {
                                        print "File ".f5." does not exist"
                                        quit
                                    }
                                }
                            }
                            else {
                                print "File ".f4." does not exist"
                                quit
                            }
                        }
                    }
                    else {
                        print "File ".f3." does not exist"
                        quit
                    }
                }
            }
            else {
                print "File ".f2." does not exist"
                quit
            }
        }
    }
    else {
        print "File ".f1." does not exist"
        quit
    }
}
else {
    print "At least one file must be given: gnuplot -e \"f1=fileToPlot\" plot.gp" 
}

if (exists("l")) {
    set style data lines
    ypause=0
    bin(val, binwidth) = binwidth * floor(val/binwidth)
    if (exists("m")) {
        if (m eq "Real") {
            tit="Real pointclouds"
            cadaux="<grep -v -e special -e synthetic -e r_sm_tm_03.pcd -e r_sm_tm_02.pcd -e r_sm_tl_04.pcd -e r_sm_tl_03.pcd -e r_sl_tl_04.pcd -e r_sl_tl_01.pcd -e r_sh_tm_02.pcd -e r_sh_th_05.pcd -e \";1.0;\" "
        }
        else {
            tit="Synthetic pointclouds"
            cadaux="<grep -v -e special -e real -e s_sh_th_04.pcd -e s_sh_tm_04.pcd -e s_sh_tl_04.pcd -e \";1.0;\" "
        }
    }
    else {
            tit="Real pointclouds"
            cadaux="<grep -v -e special -e synthetic -e r_sm_tm_03.pcd -e r_sm_tm_02.pcd -e r_sm_tl_04.pcd -e r_sm_tl_03.pcd -e r_sl_tl_04.pcd -e r_sl_tl_01.pcd -e r_sh_tm_02.pcd -e r_sh_th_05.pcd -e \";1.0;\" "
    }

    # Entropy level: Compression rate
    set yrange [0:100]
    set ylabel "Compression rate (%)"
    set xlabel "Entropy level"
    set title tit
    if (exists("t")) {
        if (t eq "tex") {  
            set output "entropyCR.tex"
            ypause=1
        }
        if (t eq "pdf") {  
            set output "entropyCR.pdf"
            ypause=1
        }
    }
    plot for [i=1:words(files)]  cadaux.word(files,i) using (bin(column(13),0.5)):(100*$2) smooth unique t word(files,i) noenhanced
    if (ypause==0) {
        pause -1
    }

    # Curvature level: Compression rate
    set yrange [0:100]
    set ylabel "Compression rate (%)"
    set xlabel "Curvature level"
    set title tit
    if (exists("t")) {
        if (t eq "tex") {  
            set output "curvatureCR.tex"
        }
        if (t eq "pdf") {  
            set output "curvatureCR.pdf"
        }
    }
    plot for [i=1:words(files)] cadaux.word(files,i) using (bin(column(9), 0.01)):(100*$2) smooth unique t word(files,i) noenhanced
    if (ypause==0) {
        pause -1
    }

    # Entropy level: Distance error
    #set yrange [0:0.05]
    unset yrange
    set logscale y
    set ylabel "Distance error"
    set xlabel "Entropy level"
    set title tit
    if (exists("t")) {
        if (t eq "tex") {  
            set output "entropyDE.tex"
            ypause=1
        }
        if (t eq "pdf") {  
            set output "entropyDE.pdf"
            ypause=1
        }
    }
    plot for [i=1:words(files)] cadaux.word(files,i) using (bin(column(13), 0.5)):3 smooth unique t word(files,i) noenhanced
    if (ypause==0) {
        pause -1
    }

    # Curvature level: Distance error
    set ylabel "Distance error"
    set xlabel "Curvature level"
    set title tit
    if (exists("t")) {
        if (t eq "tex") {  
            set output "curvatureDE.tex"
        }
        if (t eq "pdf") {  
            set output "curvatureDE.pdf"
        }
    }
    plot for [i=1:words(files)] cadaux.word(files,i) using (bin(column(9), 0.01)):3 smooth unique t word(files,i) noenhanced
    if (ypause==0) {
        pause -1
    }

    # Entropy level: Color error
    unset yrange    
    set logscale y
    set ylabel "Color error"
    set xlabel "Entropy level"
    set title tit
    if (exists("t")) {
        if (t eq "tex") {  
            set output "entropyCE.tex"
            ypause=1
        }
        if (t eq "pdf") {  
            set output "entropyCE.pdf"
            ypause=1
        }
    }
    plot for [i=1:words(files)] "<grep -v -e special -e synthetic  ".word(files,i) using (bin(column(13), 0.5)):5 smooth unique t word(files,i) noenhanced
    if (ypause==0) {
        pause -1
    }

    # Curvature level: Color error
    set ylabel "Color error"
    set xlabel "Curvature level"
    set title tit
    if (exists("t")) {
        if (t eq "tex") {  
            set output "curvatureCE.tex"
        }
        if (t eq "pdf") {  
            set output "curvatureCE.pdf"
        }
    }
    plot for [i=1:words(files)] "<grep -v -e special -e synthetic  ".word(files,i) using (bin(column(9), 0.01)):5 smooth unique t word(files,i) noenhanced
    if (ypause==0) {
        pause -1
    }
}
else {
    max_color=0
    max_distance=0
    min_color=10000
    min_distance=10000
    do for [i=1:words(files)] {
        file=word(files, i)
        set print file.".stats"
        if (exists("m")) {
            if (m eq "CO TE") {
                stats "<grep Texture_high ".file using 2 name "COMPRATE" nooutput
                stats "<grep Texture_high ".file using 3 name "DISTANCE" nooutput
                stats "<grep Texture_high ".file using 5 name "TEXTURE" nooutput
                print 1," \"Texture High\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
                stats "<grep Texture_medium ".file using 2 name "COMPRATE" nooutput
                stats "<grep Texture_medium ".file using 3 name "DISTANCE" nooutput
                stats "<grep Texture_medium ".file using 5 name "TEXTURE" nooutput
                print 2," \"Texture Medium\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
                stats "<grep Texture_low ".file using 2 name "COMPRATE" nooutput
                stats "<grep Texture_low ".file using 3 name "DISTANCE" nooutput
                stats "<grep Texture_low ".file using 5 name "TEXTURE" nooutput
                print 3," \"Texture Low\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
            }
            else {
                if (m eq "CO ST") {
                    stats "<grep Structure_high ".file using 2 name "COMPRATE" nooutput
                    stats "<grep Structure_high ".file using 3 name "DISTANCE" nooutput
                    stats "<grep Structure_high ".file using 5 name "TEXTURE" nooutput
                    print 1," \"Structure High\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                    if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                    if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                    if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                    if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
                    stats "<grep Structure_medium ".file using 2 name "COMPRATE" nooutput
                    stats "<grep Structure_medium ".file using 3 name "DISTANCE" nooutput
                    stats "<grep Structure_medium ".file using 5 name "TEXTURE" nooutput
                    print 2," \"Structure Medium\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                    if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                    if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                    if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                    if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
                    stats "<grep Structure_low ".file using 2 name "COMPRATE" nooutput
                    stats "<grep Structure_low ".file using 3 name "DISTANCE" nooutput
                    stats "<grep Structure_low ".file using 5 name "TEXTURE" nooutput
                    print 3," \"Structure Low\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                    if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                    if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                    if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                    if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
                }
                else {
                    if (m eq "SY TE") {
                        stats "<grep \"synthetic.*Texture_high\" ".file  using 2 name "COMPRATE" nooutput
                        stats "<grep \"synthetic.*Texture_high\" ".file using 3 name "DISTANCE" nooutput
                        stats "<grep \"synthetic.*Texture_high\" ".file using 5 name "TEXTURE" nooutput
                        print 1," \"Texture High\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                        if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                        if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                        if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                        if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
                        stats "<grep \"synthetic.*Texture_medium\" ".file using 2 name "COMPRATE" nooutput
                        stats "<grep \"synthetic.*Texture_medium\" ".file using 3 name "DISTANCE" nooutput
                        stats "<grep \"synthetic.*Texture_medium\" ".file using 5 name "TEXTURE" nooutput
                        print 2," \"Texture Medium\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                        if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                        if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                        if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                        if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
                        stats "<grep \"synthetic.*Texture_low\" ".file using 2 name "COMPRATE" nooutput
                        stats "<grep \"synthetic.*Texture_low\" ".file using 3 name "DISTANCE" nooutput
                        stats "<grep \"synthetic.*Texture_low\" ".file using 5 name "TEXTURE" nooutput
                        print 3," \"Texture Low\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                        if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                        if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                        if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                        if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
                    }
                    else {
                        if (m eq "SY ST") {
                            stats "<grep \"synthetic.*Structure_high\" ".file  using 2 name "COMPRATE" nooutput
                            stats "<grep \"synthetic.*Structure_high\" ".file using 3 name "DISTANCE" nooutput
                            stats "<grep \"synthetic.*Structure_high\" ".file using 5 name "TEXTURE" nooutput
                            print 1," \"Structure High\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                            if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                            if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                            if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                            if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
                            stats "<grep \"synthetic.*Structure_medium\" ".file using 2 name "COMPRATE" nooutput
                            stats "<grep \"synthetic.*Structure_medium\" ".file using 3 name "DISTANCE" nooutput
                            stats "<grep \"synthetic.*Structure_medium\" ".file using 5 name "TEXTURE" nooutput
                            print 2," \"Structure Medium\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                            if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                            if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                            if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                            if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
                            stats "<grep \"synthetic.*Structure_low\" ".file using 2 name "COMPRATE" nooutput
                            stats "<grep \"synthetic.*Structure_low\" ".file using 3 name "DISTANCE" nooutput
                            stats "<grep \"synthetic.*Structure_low\" ".file using 5 name "TEXTURE" nooutput
                            print 3," \"Structure Low\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                            if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                            if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                            if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                            if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
                        }
                        else {
                            if (m eq "RE TE") {
                                stats "<grep \"real.*Texture_high\" ".file  using 2 name "COMPRATE" nooutput
                                stats "<grep \"real.*Texture_high\" ".file using 3 name "DISTANCE" nooutput
                                stats "<grep \"real.*Texture_high\" ".file using 5 name "TEXTURE" nooutput
                                print 1," \"Texture High\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                                if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                                if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                                if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                                if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
                                stats "<grep \"real.*Texture_medium\" ".file using 2 name "COMPRATE" nooutput
                                stats "<grep \"real.*Texture_medium\" ".file using 3 name "DISTANCE" nooutput
                                stats "<grep \"real.*Texture_medium\" ".file using 5 name "TEXTURE" nooutput
                                print 2," \"Texture Medium\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                                if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                                if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                                if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                                if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
                                stats "<grep \"real.*Texture_low\" ".file using 2 name "COMPRATE" nooutput
                                stats "<grep \"real.*Texture_low\" ".file using 3 name "DISTANCE" nooutput
                                stats "<grep \"real.*Texture_low\" ".file using 5 name "TEXTURE" nooutput
                                print 3," \"Texture Low\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                                if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                                if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                                if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                                if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
                            }
                            else {
                                if (m eq "RE ST") {
                                    stats "<grep \"real.*Structure_high\" ".file  using 2 name "COMPRATE" nooutput
                                    stats "<grep \"real.*Structure_high\" ".file using 3 name "DISTANCE" nooutput
                                    stats "<grep \"real.*Structure_high\" ".file using 5 name "TEXTURE" nooutput
                                    print 1," \"Structure High\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                                    if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                                    if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                                    if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                                    if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
                                    stats "<grep \"real.*Structure_medium\" ".file using 2 name "COMPRATE" nooutput
                                    stats "<grep \"real.*Structure_medium\" ".file using 3 name "DISTANCE" nooutput
                                    stats "<grep \"real.*Structure_medium\" ".file using 5 name "TEXTURE" nooutput
                                    print 2," \"Structure Medium\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                                    if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                                    if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                                    if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                                    if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
                                    stats "<grep \"real.*Structure_low\" ".file using 2 name "COMPRATE" nooutput
                                    stats "<grep \"real.*Structure_low\" ".file using 3 name "DISTANCE" nooutput
                                    stats "<grep \"real.*Structure_low\" ".file using 5 name "TEXTURE" nooutput
                                    print 3," \"Structure Low\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
                                    if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
                                    if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
                                    if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
                                    if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
                                }
                                else {
                                    print "Option m=\"".m."\" is not correct. Use any combination of CO|SY|RE TE|ST"
                                    quit
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            stats file using 2 name "COMPRATE" nooutput
            stats file using 3 name "DISTANCE" nooutput
            stats file using 5 name "TEXTURE" nooutput
            print 1," \"Complete\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
            if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
            if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
            if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
            if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
            stats "<grep synthetic ".file using 2 name "COMPRATE" nooutput
            stats "<grep synthetic ".file using 3 name "DISTANCE" nooutput
            stats "<grep synthetic ".file using 5 name "TEXTURE" nooutput
            print 2," \"Synthetic\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
            if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
            if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
            if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
            if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
            stats "<grep real ".file using 2 name "COMPRATE" nooutput
            stats "<grep real ".file using 3 name "DISTANCE" nooutput
            stats "<grep real ".file using 5 name "TEXTURE" nooutput
            print 3," \"Real\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
            if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
            if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
            if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
            if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
            stats "<grep special ".file using 2 name "COMPRATE" nooutput
            stats "<grep special ".file using 3 name "DISTANCE" nooutput
            stats "<grep special ".file using 5 name "TEXTURE" nooutput
            print 4," \"Special\" ",COMPRATE_mean,COMPRATE_stddev,DISTANCE_mean,DISTANCE_stddev,TEXTURE_mean,TEXTURE_stddev
            if (max_color<TEXTURE_mean+TEXTURE_stddev) {max_color=TEXTURE_mean+TEXTURE_stddev}
            if (max_distance<DISTANCE_mean+DISTANCE_stddev) {max_distance=DISTANCE_mean+DISTANCE_stddev}
            if (min_color>TEXTURE_mean-TEXTURE_stddev) {min_color=TEXTURE_mean-TEXTURE_stddev}
            if (min_distance>DISTANCE_mean-DISTANCE_stddev) {min_distance=DISTANCE_mean-DISTANCE_stddev}
        }
        unset print
    }

    if (min_distance>0) {min_distance=0}
    if (min_color>0) {min_color=0}
    set datafile separator " "
    set xtics rotate
    set style histogram errorbars gap 2
    set style data histograms
    ypause=0

    # Compression rate
    set yrange [0:100]
    set ylabel "Compression rate (%)"
    if (exists("t")) {
        if (t eq "tex") {  
            set output "comprate.tex"
            ypause=1
        }
        if (t eq "pdf") {  
            set output "comprate.pdf"
            ypause=1
        }
    }
    plot for [i=1:words(files)] word(files,i).'.stats' using (100*$3):(100*$4):xtic(2) t word(files,i) noenhanced
    if (ypause==0) {
        pause -1
    }

    # Distance error
    if (max_distance==0) {
        max_distance=1
    }
    #set yrange [0:1.1*max_distance]
    unset yrange
    set logscale y
    set ylabel "Distance error (m)"
    if (exists("t")) {
        if (t eq "tex") {  
            set output "distance.tex"
        }
        if (t eq "pdf") {  
            set output "distance.pdf"
        }
    }
    plot for [i=1:words(files)] word(files,i).'.stats' using 5:6:xtic(2) t word(files,i) noenhanced
    if (ypause==0) {
        pause -1
    }

    # Color error
    #set yrange [0:max_color+1]
    set ylabel "Texture error"
    if (exists("t")) {
        if (t eq "tex") {  
            set output "texture.tex"
        }
        if (t eq "pdf") {  
            set output "texture.pdf"
        }
    }
    plot for [i=1:words(files)] word(files,i).'.stats' using 7:8:xtic(2) t word(files,i) noenhanced
    if (ypause==0) {
        pause -1
    }
}

quit
